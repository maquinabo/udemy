//Atividade 1
function ola(n){
    return console.log(`Olá, ${n}!`)
}
ola("Tekete")
console.log("----------------------------------")

//Atividade 2
function anoDia(n){
    return console.log(`${n} anos seriam ${n*365} dias`)
}
anoDia(22)
console.log("----------------------------------")

//Atividade 3
function Salario(horaTrab, recebeHora){
    return console.log(`Você trabalha ${horaTrab}h por mês e recebe R$${recebeHora}/h, então seu salário será de R$${horaTrab*recebeHora}/mês`)
}
Salario(150,40.5)
console.log("----------------------------------")

//Atividade 4
function nomeDoMes(n){
    if(n == 1){
        return console.log(`O Mês ${n} é Janeiro`)
    }else if(n == 2){
        return console.log(`O Mês ${n} é Fevereiro`)
    }else if(n == 3){
        return console.log(`O Mês ${n} é Março`)
    }else if(n == 4){
        return console.log(`O Mês ${n} é Abril`)
    }else if(n == 5){
        return console.log(`O Mês ${n} é Maio`)
    }else if(n == 6){
        return console.log(`O Mês ${n} é Junho`)
    }else if(n == 7){
        return console.log(`O Mês ${n} é Julho`)
    }else if(n == 8){
        return console.log(`O Mês ${n} é Agosto`)
    }else if(n == 9){
        return console.log(`O Mês ${n} é Setembro`)
    }else if(n == 10){
        return console.log(`O Mês ${n} é Outubro`)
    }else if(n == 11){
        return console.log(`O Mês ${n} é Novembro`)
    }else if(n == 12){
        return console.log(`O Mês ${n} é Dezembro`)
    }else if(n < 1 || n > 12){
        return console.log(`Esse não é um Mês válido`)
    }
}
nomeDoMes(3)
console.log("----------------------------------")

//Atividade 5
function maiorOuIgual(n1, n2){
    if(n1 > n2){
        console.log(`${n1} é maior que ${n2}`)
    }else if(n1 < n2){
        console.log(`${n1} é menor que ${n2}`)
    }else if(n1 === n2){
        console.log(`${n1} é igual a ${n2}`)
    }else{
        console.log(`Os dados fornecidos não são números!`)
    }
}
maiorOuIgual(2,1)
maiorOuIgual(2,3)
maiorOuIgual(2,2)
maiorOuIgual(2,"Oi")
console.log("----------------------------------")

//Atividade 6
function inverso(n){
    if(n >= 0){
        console.log(`O inverso de ${n} é ${-n}`)
    }else if(n < 0){
        console.log(`O inverso de ${n} é ${n*-1}`)
    }else{
        console.log(`O dado fornecido não é um número!`)
    }
}
inverso(10)
inverso(-10)
inverso("Oi")
console.log("----------------------------------")

//Atividade 7
function estaEntre(n, minimo, maximo, inclusivo = false){
    if(minimo < n && n < maximo){
        console.log(`${n} está entre ${minimo} e ${maximo}`)
    }else if(minimo > n){
        console.log(`${n} não está entre ${minimo} e ${maximo}`)
    }else if(minimo == n && inclusivo == true || n == maximo && inclusivo == true){
        console.log(`${n} está entre ${minimo} e ${maximo}`)
    }else{
        console.log(`${n} não está entre ${minimo} e ${maximo}`)
    }
}
estaEntre(5,1,10)
estaEntre(1,2,10)
estaEntre(5,5,10,true)
estaEntre(5,5,10)
console.log("----------------------------------")

//Atividade 8
function multiplicar(n1, n2){
    let count = n2
    let resultado = 0
    while(count > 0){
        resultado += n1
        count--
    }
    console.log(`${n1}x${n2}=${resultado}`)
}
multiplicar(5,5)
multiplicar(0,7)
console.log("----------------------------------")

//Atividade 9
function repetir(n1, n2){
    let repetirArray = []
    while(n2 > 0){
        repetirArray.push(n1)
        n2--
    }
    console.log(repetirArray)
}
repetir("Oi",2)
repetir(7,3)
console.log("----------------------------------")

//Atividade 10
function simboloMais(n){
    let mais = "+"
    let resultado = ""
    while(n > 0){
        resultado += mais
        n--
    }
    console.log(resultado)
}
simboloMais(2)
simboloMais(5)
console.log("----------------------------------")

//Atividade 11
function receberPrimeiroEUltimoElemento(n){
    let newN = [n[0],n[n.length-1]]
    console.log(newN)
}
receberPrimeiroEUltimoElemento([7,14,"Olá"])
receberPrimeiroEUltimoElemento([-100, "aplicativo", 16])
console.log("----------------------------------")

//Atividade 12
function removerPropriedade(dados, exclusao){
    let copia = {...dados}
    delete copia[exclusao] 
    console.log(copia)
}
removerPropriedade({a: 1, b: 2}, "a")
removerPropriedade({
    id: 20,
    nome: "caneta",
    descricao: "Não preenchido"
    }, "descricao")
console.log("----------------------------------")

//Atividade 13
function filtrarNumeros(n){
    console.log(n.filter(Number.isInteger))
}
filtrarNumeros(["Javascript", 1, "3", "Web", 20])
filtrarNumeros(["a", "c"])
console.log("----------------------------------")

//Atividade 14
function objetoParaArray(array){
    let newArray = Object.entries(array)
    console.log(newArray)
}
objetoParaArray({
    nome: "Maria",
    profissao: "Desenvolvedora de software"
    }) 
objetoParaArray({
    codigo: 11111,
    preco: 12000
    })     
console.log("----------------------------------")

//Atividade 15
function receberSomenteOsParesDeIndicesPares(array){
    const newArray = array.filter((e, i) => e%2 == 0 && i%2 == 0)
    console.log(newArray)
}
receberSomenteOsParesDeIndicesPares([1, 2, 3, 4])
receberSomenteOsParesDeIndicesPares([10, 70, 22, 43])
console.log("----------------------------------")

//Atividade 16
